package jcode.project.base;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 基本方法
 * Created by Administrator on 2017/7/5.
 */
public class Utils {

    public static <T,R> List<R> map(List<T> source, java.util.function.Function<T,R> function){
        return source.stream().map(function).collect(Collectors.toList());
    }

    public static boolean isEmpty(List list){
        return list == null || list.isEmpty();
    }

}
